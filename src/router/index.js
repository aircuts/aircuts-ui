import Vue from 'vue'
import VueRouter from 'vue-router'
import ProviderList from "@/views/ProviderListView";
import ProviderView from "@/views/ProviderView";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'ProviderList',
    component: ProviderList
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/provider',
    name: 'ProviderView',
    component: ProviderView,
    props:{
      default: true
    }
  }
]

const router = new VueRouter({
  routes
})

export default router
