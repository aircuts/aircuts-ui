import axios from 'axios'

const apiClient = axios.create({
    baseURL: 'http://localhost:8082'
})

export default {

    getProviders(){
        return apiClient.get('/providers/')
    },

    getProvider(providerId){
        return apiClient.get('/providers/' + providerId)
    }
}
