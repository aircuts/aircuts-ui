import Vue from 'vue'
import Vuex from 'vuex'
import * as provider from './modules/provider'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    provider
  }
})
